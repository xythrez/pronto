#include <unistd.h>
#include <stdio.h>
#include <libpmem.h>
#include <vector>
#include <chrono>
#include <tbb/concurrent_unordered_map.h>
#include "persister.hpp"
#include "nv_object.hpp"
#include "thread.hpp"

#ifdef DEBUG
static inline uint64_t rdtscp() {
  uint32_t aux;
  uint64_t rax;
  rax = __rdtscp(&aux);
  return rax;
}
#endif

// extern uint64_t num_active_sync;
// extern pthread_mutex_t sync_lock;
// extern pthread_cond_t sync_sig;
uint64_t savitar_num_workers = 0;

void *Savitar_persister_worker(void *arg) {
    // num_active_sync = 0;
    // assert(pthread_mutex_init(&sync_lock, NULL) == 0);
    // assert(pthread_cond_init(&sync_sig, NULL) == 0);
    tbb::concurrent_unordered_map<uint64_t, VolatileInfo> &logical_info =
            *NVManager::getInstance().getVolatileMetadata();
    uint64_t prefix;
    char *start;
    char *ptr;
    while (savitar_num_workers != 0) { // While there are still workers running
        auto iter = logical_info.begin();
        for (; iter != logical_info.end(); iter++) {
            SavitarLog *log = (SavitarLog *) iter->first;
            VolatileInfo *info = &iter->second;

            // if difference is smaller than sleep ms
            if (info->ts > std::chrono::high_resolution_clock::now()) {
                assert(usleep((PERSISTER_IDLE_US) / savitar_num_workers) == 0);
                continue;
            }
            info->ts = std::chrono::high_resolution_clock::now() + 
                std::chrono::microseconds(PERSISTER_IDLE_US);

            start = ((char *) log) + log->tail;
            ptr = start;
            while (*((uint64_t *) ptr) != 0) {
                ptr += 2 * sizeof(uint64_t);
                ptr += *((uint64_t *) ptr);
            }

            prefix = (uint64_t) ptr - (uint64_t) log;
            if (prefix > log->tail) {
                pmem_flush(start, ptr - start);
                log->tail = prefix;
                log->last_commit = info->logical_commit;
                pmem_flush(&log->tail, 2 * sizeof(uint64_t));
                pmem_drain();
            }

            __sync_synchronize();
            PRINT("[%d] Persisted log %p to up to tail %lx and commit %lu\n",
                  0, log, log->tail, log->last_commit);
            // if (num_active_sync > 0) assert(pthread_cond_broadcast(&sync_sig) == 0);
        }
    }

    PRINT("[%d] Received TERM signal for the persister thread\n", 0);
    // Finish persist before exit
    auto iter = logical_info.begin();
    for (; iter != logical_info.end(); iter++) {
        SavitarLog *log = (SavitarLog *) iter->first;
        VolatileInfo *info = &iter->second;

        start = ((char *) log) + log->tail;
        ptr = start;
        while (*((uint64_t *) ptr) != 0) {
            ptr += 2 * sizeof(uint64_t);
            ptr += *((uint64_t *) ptr);
        }

        prefix = (uint64_t) ptr  - (uint64_t) log;
        if (prefix > log->tail) {
            pmem_flush(start, ptr - start);
            log->tail = prefix;
            log->last_commit = info->logical_commit;
            pmem_flush(&log->tail, 2 * sizeof(uint64_t));
            pmem_drain();
        }
    }

    // assert(pthread_mutex_destroy(&sync_lock) == 0);
    // assert(pthread_cond_destroy(&sync_sig) == 0);
    return NULL;
}
