#ifndef PERSISTENT_NBQUEUE_HPP_
#define PERSISTENT_NBQUEUE_HPP_

#include <atomic>
#include "../common/base.hpp"

using namespace std;

class PersistentNBQueue : public PersistentObject {
public:
    typedef int T;
    struct QueueNode;

    struct QueuePtr {
        QueueNode *ptr;
        unsigned int count;
        bool operator==(const QueuePtr &y){
            return ptr == y.ptr && count == y.count;
        }
    };

    struct QueueNode {
        T value = -1;
        atomic<QueuePtr> next;
    };

    PersistentNBQueue(uuid_t id) : PersistentObject(id) {
        QueueNode *node = new QueueNode();
        node->next = {NULL, 0};
        head = {node, 0};
        tail = {node, 0};
    }

    void prefill(T value) {
        QueueNode *node = new QueueNode();
        QueuePtr last_tail;
        QueuePtr next;
        node->value = value;
        node->next = {NULL, 0};
        while (1) {
            last_tail = tail;
            next = last_tail.ptr->next;
            if (last_tail == tail) {
                if (next.ptr == NULL) {
                    if (tail.load().ptr->next.compare_exchange_strong(next,
                            {node, next.count + 1})) break;
                } else {
                    tail.compare_exchange_weak(last_tail,
                        {next.ptr, last_tail.count + 1});
                }
            }
        }
        tail.compare_exchange_weak(last_tail, {node, last_tail.count + 1});
    }

    void push(T value) {
        // <compiler>
        Savitar_thread_notify(3, this, PushTag, &value);
        // </compiler>
        QueueNode *node = new QueueNode();
        QueuePtr last_tail;
        QueuePtr next;
        node->value = value;
        node->next = {NULL, 0};
        while (1) {
            last_tail = tail;
            next = last_tail.ptr->next;
            if (last_tail == tail) {
                if (next.ptr == NULL) {
                    if (tail.load().ptr->next.compare_exchange_strong(next,
                            {node, next.count + 1})) {
                        // <compiler>
                        Savitar_thread_wait(this, this->log);
                        // </compiler>
                        break;
                    }
                } else {
                    tail.compare_exchange_weak(last_tail,
                        {next.ptr, last_tail.count + 1});
                }
            }
        }
        tail.compare_exchange_weak(last_tail, {node, last_tail.count + 1});
    }

    T pop() {
        // <compiler>
        Savitar_thread_notify(2, this, PopTag);
        // </compiler>
        QueuePtr last_head;
        QueuePtr last_tail;
        QueuePtr next;
        T val;
        while (1) {
            last_head = head;
            last_tail = tail;
            next = last_head.ptr->next;
            if (last_head == head) {
                if (last_head.ptr == last_tail.ptr) {
                    if (next.ptr == NULL) {
                        // <compiler>
                        Savitar_thread_wait(this, this->log);
                        // </compiler>
                        return -1;
                    }
                    tail.compare_exchange_weak(last_tail,
                        {next.ptr, last_tail.count + 1});
                } else {
                    val = next.ptr->value;
                    if (head.compare_exchange_strong(last_head,
                            {next.ptr, last_head.count + 1})) {
                        // <compiler>
                        Savitar_thread_wait(this, this->log);
                        // </compiler>
                        break;
                    }
                }
            }
        }
        free(last_head.ptr);
        return val;
    }

    T peek() {
        return head.load().ptr->value;
    }

    // <compiler>
    PersistentNBQueue() : PersistentObject(true) {}

    static PersistentObject *BaseFactory(uuid_t id) {
        ObjectAlloc *alloc = GlobalAlloc::getInstance()->newAllocator(id);
        PersistentNBQueue *object = new PersistentNBQueue(id);
        return object;
    }

    static PersistentObject *RecoveryFactory(NVManager *m, CatalogEntry *e) {
        return BaseFactory(e->uuid);
    }

    static PersistentNBQueue *Factory(uuid_t id) {
        NVManager &manager = NVManager::getInstance();
        manager.lock();
        PersistentNBQueue *obj = (PersistentNBQueue *)manager.findRecovered(id);
        if (obj == NULL) {
            obj = static_cast<PersistentNBQueue *>(BaseFactory(id));
            manager.createNew(classID(), obj);
        }
        manager.unlock();
        return obj;
    }


    uint64_t Log(uint64_t tag, uint64_t *args) {
        int vector_size = 0;
        ArgVector vector[4]; // Max arguments of the class

        switch (tag) {
            case PushTag:
                {
                vector[0].addr = &tag;
                vector[0].len = sizeof(tag);
                vector[1].addr = (int *)args[0];
                vector[1].len = sizeof(int);
                vector_size = 2;
                }
                break;
            case PopTag:
                {
                vector[0].addr = &tag;
                vector[0].len = sizeof(tag);
                vector_size = 1;
                }
                break;
            default:
                assert(false);
                break;
        }

        return AppendLog(vector, vector_size);
    }

    size_t Play(uint64_t tag, uint64_t *args, bool dry) {
        size_t bytes_processed = 0;
        switch (tag) {
        case PushTag:
            {
            if (!dry) push(*((int *)args));
            bytes_processed = 2 * sizeof(int);
            break;
            }
        case PopTag:
            {
            if (!dry) pop();
            bytes_processed = sizeof(int);
            break;
            }
        default:
            {
            PRINT("Unknown tag: %zu\n", tag);
            assert(false);
            break;
            }
        }
        return bytes_processed;
    }

    // <compiler>
    static uint64_t classID() { return 1; }

private:
    atomic<QueuePtr> head;
    atomic<QueuePtr> tail;
    // <compiler>
    enum MethodTags {
        PushTag = 1,
        PopTag = 2,
    };
    // </compiler>
};


class NvNBQueueBenchmark : public Benchmark {
public:
    virtual const char *name() {
        return "NvNBQueue";
    }

    virtual void register_factory() {
        PersistentFactory::registerFactory<PersistentNBQueue>();
    }

    virtual void init() {
        uuid_t uuid;
        uuid_generate(uuid);
        ds = PersistentNBQueue::Factory(uuid);
        assert(ds != NULL);
    }

    virtual void do_prefill(int key) {
        ds->prefill(key);
    }

    virtual void do_insert(int key) {
        ds->push(key);
    }

    virtual void do_delete(int key) {
        ds->pop();
    }

    virtual void do_lookup(int key) {
        ds->peek();
    }

    virtual void do_sync() {
        Savitar_thread_sync();
    }

    virtual bool use_pronto() {
        return true;
    }

protected:
    PersistentNBQueue *ds;
};

#endif  // PERSISTENT_NBQUEUE_HPP_
