#ifndef PERSISTENT_MONTAGENBQUEUE_HPP_
#define PERSISTENT_MONTAGENBQUEUE_HPP_

#include <atomic>
#include <pthread.h>
#include "persist/api/Recoverable.hpp"
#include "../common/base.hpp"

using namespace std;

class MontageNBQueue : public Recoverable {
public:
    typedef int T;
    struct QueueNode;

    class Payload : public pds::PBlk {
        GENERATE_FIELD(int, index, Payload);
        GENERATE_FIELD(T, val, Payload);
      public:
        Payload(){}
        Payload(int x, T y): m_index(x), m_val(y){}
        Payload(const Payload& oth): pds::PBlk(oth), m_index(oth.m_index), m_val(oth.m_val){}
        void persist(){}
    };

    struct QueuePtr {
        QueueNode *ptr;
        unsigned int count;
        bool operator==(const QueuePtr &y){
            return ptr == y.ptr && count == y.count;
        }
    };

    struct QueueNode {
        MontageNBQueue *ds;
        Payload *payload;
        atomic<QueuePtr> next;
        QueueNode(){}
        QueueNode(MontageNBQueue *queue, int index, T value): ds(queue) {
            payload = queue->pnew<Payload>(index, value);
            next = {NULL, 0};
        }
        QueueNode(Payload *pl) : payload(pl) {}
        int get_index(){
            return (int) payload->get_unsafe_index(ds);
        }
        void set_index(int index){
            payload = payload->set_index(ds, index);
        }
        T get_val(){
            return (T) payload->get_unsafe_val(ds);
        }
        void set_val(T val){
            payload = payload->set_val(ds, val);
        }
        ~QueueNode() {
            if (payload) ds->pdelete(payload);
        }
    };

    GlobalTestConfig *get_gtc(int threads) {
        GlobalTestConfig *gtc = new GlobalTestConfig;
        if (threads == 5 || threads == 9 || threads == 17 || threads == 33)
            threads++;
        gtc->task_num = threads;
        hwloc_topology_init(&(gtc->topology));
        hwloc_topology_load(gtc->topology);
        gtc->num_procs = hwloc_get_nbobjs_by_depth(gtc->topology,
            hwloc_get_type_depth(gtc->topology, HWLOC_OBJ_PU));
        gtc->buildAffinity(gtc->affinities);
        gtc->recorder = new Recorder(gtc->task_num);
        return gtc;
    }

    MontageNBQueue(int threads): Recoverable(get_gtc(threads)) {
        qindex = 0;
        QueueNode *node = new QueueNode(this, -1, -1);
        head = {node, 0};
        tail = {node, 0};
    };

    void prefill(T value) {
        push(value);
    }

    void push(T value) {
        MontageOpHolder _holder(this);
        QueueNode *node = new QueueNode(this, -1, value);
        QueuePtr last_tail;
        QueuePtr next;
        while (1) {
            last_tail = tail;
            next = last_tail.ptr->next;
            if (last_tail == tail) {
                if (next.ptr == NULL) {
                    if (tail.load().ptr->next.compare_exchange_strong(next,
                            {node, next.count + 1})) {
                        int index = qindex.fetch_add(1);
                        node->set_index(index);
                        break;
                    }
                } else {
                    tail.compare_exchange_weak(last_tail,
                        {next.ptr, last_tail.count + 1});
                }
            }
        }
        tail.compare_exchange_weak(last_tail, {node, last_tail.count + 1});
    }

    T pop() {
        QueuePtr last_head;
        QueuePtr last_tail;
        QueuePtr next;
        T val;
        MontageOpHolder _holder(this);
        while (1) {
            last_head = head;
            last_tail = tail;
            next = last_head.ptr->next;
            if (last_head == head) {
                if (last_head.ptr == last_tail.ptr) {
                    if (next.ptr == NULL) {
                        return -1;
                    }
                    tail.compare_exchange_weak(last_tail,
                        {next.ptr, last_tail.count + 1});
                } else {
                    val = next.ptr->get_val();
                    if (head.compare_exchange_strong(last_head,
                            {next.ptr, last_head.count + 1})) {
                        break;
                    }
                }
            }
        }
        free(last_head.ptr);
        return val;
    }

    T peek() {
        MontageOpHolderReadOnly(this);
        return head.load().ptr->get_val();
    }
    
    int recover(bool simulated = false) {
        return 0;
    }

private:
    atomic<QueuePtr> head;
    atomic<QueuePtr> tail;
    atomic<int> qindex;
};


class MontageNBQueueBenchmark : public Benchmark {
public:
    virtual const char *name() {
        return "MontageNBQueue";
    }

    virtual void init() {
        ds = new MontageNBQueue(cfg->threads);
    }

    virtual void do_prefill(int key) {
        ds->prefill(key);
    }

    virtual void do_insert(int key) {
        ds->push(key);
    }

    virtual void do_delete(int key) {
        ds->pop();
    }

    virtual void do_lookup(int key) {
        ds->peek();
    }

    virtual void do_sync() {
        ds->sync();
    }

    virtual void post_init(int tid) {
        int coreid = (tid % 20) * 2 + (tid / 20);
        pds::EpochSys::init_thread(tid);
        cpu_set_t cpuset;
        CPU_ZERO(&cpuset);
        CPU_SET(coreid, &cpuset);
        pthread_setaffinity_np(pthread_self(), sizeof(cpuset), &cpuset);
    }

protected:
    MontageNBQueue *ds;
};

#endif  // PERSISTENT_MONTAGENBQUEUE_HPP_
