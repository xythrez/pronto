#ifndef PERSISTENT_VECTOR_HPP_
#define PERSISTENT_VECTOR_HPP_

#include <vector>
#include "../../src/savitar.hpp"
#include "../common/base.hpp"

using namespace std;

class PersistentVector : public PersistentObject {
public:
    typedef int T;

    PersistentVector(uuid_t id) : PersistentObject(id) {
        //void *t = alloc->alloc(sizeof(vector<T, STLAlloc<T>>));
        //vector<T, STLAlloc<T>> *obj = (vector<T, STLAlloc<T>> *)t;
        //v_vector = new(obj) vector<T, STLAlloc<T>>((STLAlloc<T>(alloc)));
        v_vector = new vector<T>();
    }

    void prefill(T value) {
        v_vector->push_back(value);
    }

    void push_back(T value) {
        // <compiler>
        Savitar_thread_notify(3, this, PushBackTag, &value);
        // </compiler>
        v_vector->push_back(value);
        // <compiler>
        Savitar_thread_wait(this, this->log);
        // </compiler>
    }

    void erase(T value) {
        // <compiler>
        Savitar_thread_notify(3, this, EraseTag, &value);
        // </compiler>
        v_vector->erase(v_vector->begin()+value);
        // <compiler>
        Savitar_thread_wait(this, this->log);
        // </compiler>
    }

    void lookup(size_t idx) {
        (*v_vector)[idx];
    }

    int size() {
        return v_vector->size();
    }

    // <compiler>
    PersistentVector() : PersistentObject(true) {}

    static PersistentObject *BaseFactory(uuid_t id) {
        ObjectAlloc *alloc = GlobalAlloc::getInstance()->newAllocator(id);
        //void *temp = alloc->alloc(sizeof(PersistentVector));
        //PersistentVector *obj = (PersistentVector *)temp;
        //PersistentVector *object = new (obj) PersistentVector(id);
        PersistentVector *object = new PersistentVector(id);
        return object;
    }

    static PersistentObject *RecoveryFactory(NVManager *m, CatalogEntry *e) {
        return BaseFactory(e->uuid);
    }

    static PersistentVector *Factory(uuid_t id) {
        NVManager &manager = NVManager::getInstance();
        manager.lock();
        PersistentVector *obj = (PersistentVector *)manager.findRecovered(id);
        if (obj == NULL) {
            obj = static_cast<PersistentVector *>(BaseFactory(id));
            manager.createNew(classID(), obj);
        }
        manager.unlock();
        return obj;
    }


    uint64_t Log(uint64_t tag, uint64_t *args) {
        int vector_size = 0;
        ArgVector vector[4]; // Max arguments of the class

        switch (tag) {
            case PushBackTag:
                {
                vector[0].addr = &tag;
                vector[0].len = sizeof(tag);
                vector[1].addr = (void *)args[0];
                vector[1].len = sizeof(int);
                vector_size = 2;
                }
                break;
            case EraseTag:
                {
                vector[0].addr = &tag;
                vector[0].len = sizeof(tag);
                vector[1].addr = (void *)args[0];
                vector[1].len = sizeof(int);
                vector_size = 2;
                }
                break;
            default:
                assert(false);
                break;
        }

        return AppendLog(vector, vector_size);
    }

    size_t Play(uint64_t tag, uint64_t *args, bool dry) {
        size_t bytes_processed = 0;
        switch (tag) {
        case PushBackTag:
            {
            int value = *((int *)args);
            if (!dry) push_back(value);
            bytes_processed = sizeof(int);
            break;
            }
        case EraseTag:
            {
            int value = *((int *)args);
            //Cannot replay unless prefill is also logged.
            //if (!dry) erase(value);
            bytes_processed = sizeof(int);
            break;
            }
        default:
            {
            PRINT("Unknown tag: %zu\n", tag);
            assert(false);
            break;
            }
        }
        return bytes_processed;
    }

    // <compiler>
    static uint64_t classID() { return 1; }

private:
    vector<T> *v_vector;
    // vector<T, STLAlloc<T>> *v_vector;
    // <compiler>
    enum MethodTags {
        PushBackTag = 1,
        EraseTag = 2,
    };
    // </compiler>
};


class NvVectorBenchmark : public Benchmark {
public:
    virtual const char *name() {
        return "NvVector";
    }

    virtual void register_factory() {
        PersistentFactory::registerFactory<PersistentVector>();
    }

    virtual void init() {
        uuid_t uuid;
        uuid_generate(uuid);
        ds = PersistentVector::Factory(uuid);
        assert(ds != NULL);
    }

    virtual void do_prefill(int key) {
        lock_guard<mutex> lock(mtx);
        ds->prefill(key);
    }

    virtual void do_insert(int key) {
        lock_guard<mutex> lock(mtx);
        ds->push_back(key);
    }

    virtual void do_delete(int key) {
        lock_guard<mutex> lock(mtx);
        if (ds->size() <= key) key = ds->size() - 1;
        ds->erase(key);
    }

    virtual void do_lookup(int key) {
        lock_guard<mutex> lock(mtx);
        if (ds->size() <= key) key = ds->size() - 1;
        ds[key];
    }

    virtual void do_sync() {
        Savitar_thread_sync();
    }

    virtual bool use_pronto() {
        return true;
    }

protected:
    PersistentVector *ds;
    mutex mtx;
};

#endif  // PERSISTENT_VECTOR_HPP_
