#ifndef PERSISTENT_HASHMAP_HPP_
#define PERSISTENT_HASHMAP_HPP_

#include "../../src/savitar.hpp"
#include "../common/base.hpp"

#define NUM_BUCKETS 64

using namespace std;

class PersistentHashMap : public PersistentObject {
public:
    typedef int T;

    struct BucketNode {
        T key = -1;
        T value = -1;
        BucketNode *next = NULL;
    };

    struct Bucket {
        std::mutex mtx;
        BucketNode head;
    } __attribute__ ((aligned (64)));

    PersistentHashMap(uuid_t id) : PersistentObject(id) {}

    void prefill(T key, T value) {
        int bid = hash(key);
        lock_guard<mutex> lock(buckets[bid].mtx);
        BucketNode *curr = &buckets[bid].head;
        while ((curr = curr->next) != NULL) {
            if (curr->key == key) break;
        }
        if (curr != NULL) {
            curr->next = new BucketNode();
            curr->next->key = key;
            curr->next->value = value;
        }
    }

    void put(T key, T value) {
        // <compiler>
        Savitar_thread_notify(4, this, PutTag, &key, &value);
        // </compiler>
        int bid = hash(key);
        lock_guard<mutex> lock(buckets[bid].mtx);
        BucketNode *curr = &buckets[bid].head;
        while ((curr = curr->next) != NULL) {
            if (curr->key == key) break;
        }
        if (curr != NULL) {
            curr->next = new BucketNode();
            curr->next->key = key;
            curr->next->value = value;
        }
        // <compiler>
        Savitar_thread_wait(this, this->log);
        // </compiler>
    }

    void remove(T key) {
        // <compiler>
        Savitar_thread_notify(3, this, RemoveTag, &key);
        // </compiler>
        int bid = hash(key);
        lock_guard<mutex> lock(buckets[bid].mtx);
        BucketNode *prev = &buckets[bid].head;
        BucketNode *curr = NULL;
        while (prev->next != NULL) {
            if (prev->next->key == key) {
                curr = prev->next;
                prev->next = curr->next;
                delete curr;
                break;
            }
        }
        // <compiler>
        Savitar_thread_wait(this, this->log);
        // </compiler>
    }

    void lookup(T key) {
        int bid = hash(key);
        BucketNode *curr = &buckets[bid].head;
        while ((curr = curr->next) != NULL) {
            if (curr->key == key) break;
        }
    }

    // <compiler>
    PersistentHashMap() : PersistentObject(true) {}

    static PersistentObject *BaseFactory(uuid_t id) {
        ObjectAlloc *alloc = GlobalAlloc::getInstance()->newAllocator(id);
        PersistentHashMap *object = new PersistentHashMap(id);
        return object;
    }

    static PersistentObject *RecoveryFactory(NVManager *m, CatalogEntry *e) {
        return BaseFactory(e->uuid);
    }

    static PersistentHashMap *Factory(uuid_t id) {
        NVManager &manager = NVManager::getInstance();
        manager.lock();
        PersistentHashMap *obj = (PersistentHashMap *)manager.findRecovered(id);
        if (obj == NULL) {
            obj = static_cast<PersistentHashMap *>(BaseFactory(id));
            manager.createNew(classID(), obj);
        }
        manager.unlock();
        return obj;
    }


    uint64_t Log(uint64_t tag, uint64_t *args) {
        int vector_size = 0;
        ArgVector vector[4]; // Max arguments of the class

        switch (tag) {
            case PutTag:
                {
                vector[0].addr = &tag;
                vector[0].len = sizeof(tag);
                vector[1].addr = (int *)args[0];
                vector[1].len = sizeof(int);
                vector[2].addr = (int *)args[1];
                vector[2].len = sizeof(int);
                vector_size = 3;
                }
                break;
            case RemoveTag:
                {
                vector[0].addr = &tag;
                vector[0].len = sizeof(tag);
                vector[1].addr = (int *)args[0];
                vector[1].len = sizeof(int);
                vector_size = 2;
                }
                break;
            default:
                assert(false);
                break;
        }

        return AppendLog(vector, vector_size);
    }

    size_t Play(uint64_t tag, uint64_t *args, bool dry) {
        size_t bytes_processed = 0;
        switch (tag) {
        case PutTag:
            {
            int key = ((int *)args)[0];
            int value = ((int *)args)[1];
            if (!dry) put(key, value);
            bytes_processed = 2 * sizeof(int);
            break;
            }
        case RemoveTag:
            {
            int key = *((int *)args);
            if (!dry) remove(key);
            bytes_processed = sizeof(int);
            break;
            }
        default:
            {
            PRINT("Unknown tag: %zu\n", tag);
            assert(false);
            break;
            }
        }
        return bytes_processed;
    }

    // <compiler>
    static uint64_t classID() { return 1; }

private:
    Bucket buckets[NUM_BUCKETS];
    int hash(T key) {return key % NUM_BUCKETS;};
    // <compiler>
    enum MethodTags {
        PutTag = 1,
        RemoveTag = 2,
    };
    // </compiler>
};


class NvHashMapBenchmark : public Benchmark {
public:
    virtual const char *name() {
        return "NvHashMap";
    }

    virtual void register_factory() {
        PersistentFactory::registerFactory<PersistentHashMap>();
    }

    virtual void init() {
        uuid_t uuid;
        uuid_generate(uuid);
        ds = PersistentHashMap::Factory(uuid);
        assert(ds != NULL);
    }

    virtual void do_prefill(int key) {
        ds->prefill(key, key);
    }

    virtual void do_insert(int key) {
        ds->put(key, key);
    }

    virtual void do_delete(int key) {
        ds->remove(key);
    }

    virtual void do_lookup(int key) {
        ds->lookup(key);
    }

    virtual void do_sync() {
        Savitar_thread_sync();
    }

    virtual bool use_pronto() {
        return true;
    }

protected:
    PersistentHashMap *ds;
};

#endif  // PERSISTENT_HASHMAP_HPP_
