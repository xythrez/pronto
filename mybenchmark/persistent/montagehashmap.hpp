#ifndef PERSISTENT_MONTAGEHASHMAP_HPP_
#define PERSISTENT_MONTAGEHASHMAP_HPP_

#include <pthread.h>
#include "persist/api/Recoverable.hpp"
#include "../common/base.hpp"

#define NUM_BUCKETS 64

using namespace std;

class MontageHashMap : public Recoverable {
public:
    typedef int T;

    class Payload : public pds::PBlk {
        GENERATE_FIELD(T, key, Payload);
        GENERATE_FIELD(T, val, Payload);
      public:
        Payload(){}
        Payload(T x, T y): m_key(x), m_val(y){}
        Payload(const Payload& oth): pds::PBlk(oth), m_key(oth.m_key), m_val(oth.m_val){}
        void persist(){}
    };

    struct BucketNode {
        MontageHashMap *ds;
        Payload *payload;
        BucketNode *next;
        BucketNode(){}
        BucketNode(MontageHashMap *map, T key, T value): ds(map) {
            payload = map->pnew<Payload>(key, value);
            next = NULL;
        }
        BucketNode(Payload *pl) : payload(pl) {}
        T get_key(){
            return (T) payload->get_unsafe_key(ds);
        }
        T get_val(){
            return (T)payload->get_unsafe_val(ds);
        }
        void set_val(T val){
            payload = payload->set_val(ds, val);
        }
        ~BucketNode() {
            if (payload) ds->pdelete(payload);
        }
    };

    struct Bucket {
        std::mutex mtx;
        BucketNode head;
        Bucket():head(){};
    } __attribute__ ((aligned (64)));

    GlobalTestConfig *get_gtc(int threads) {
        GlobalTestConfig *gtc = new GlobalTestConfig;
        if (threads == 5 || threads == 9 || threads == 17 || threads == 33)
            threads++;
        gtc->task_num = threads;
        hwloc_topology_init(&(gtc->topology));
        hwloc_topology_load(gtc->topology);
        gtc->num_procs = hwloc_get_nbobjs_by_depth(gtc->topology,
            hwloc_get_type_depth(gtc->topology, HWLOC_OBJ_PU));
        gtc->buildAffinity(gtc->affinities);
        gtc->recorder = new Recorder(gtc->task_num);
        return gtc;
    }

    MontageHashMap(int threads): Recoverable(get_gtc(threads)) {};

    void prefill(T key, T value) {
        put(key, value);
    }

    void put(T key, T value) {
        int bid = hash(key);
        lock_guard<mutex> lock(buckets[bid].mtx);
        MontageOpHolder _holder(this);
        BucketNode *curr = &buckets[bid].head;
        while ((curr = curr->next) != NULL) {
            if (curr->get_key() == key) break;
        }
        if (curr != NULL) {
            curr->next = new BucketNode(this, key, value);
        }
    }

    void remove(T key) {
        int bid = hash(key);
        lock_guard<mutex> lock(buckets[bid].mtx);
        MontageOpHolder _holder(this);
        BucketNode *prev = &buckets[bid].head;
        BucketNode *curr = NULL;
        while (prev->next != NULL) {
            if (prev->next->get_key() == key) {
                curr = prev->next;
                prev->next = curr->next;
                delete curr;
                break;
            }
        }
    }

    void lookup(T key) {
        int bid = hash(key);
        MontageOpHolderReadOnly(this);
        BucketNode *curr = &buckets[bid].head;
        while ((curr = curr->next) != NULL) {
            if (curr->get_key() == key) break;
        }
    }
    
    int recover(bool simulated = false) {
        return 0;
    }

private:
    Bucket buckets[NUM_BUCKETS];
    int hash(T key) {return key % NUM_BUCKETS;};
};

class MontageHashMapBenchmark : public Benchmark {
public:
    virtual const char *name() {
        return "MontageHashMap";
    }

    virtual void init() {
        ds = new MontageHashMap(cfg->threads);
    }

    virtual void do_prefill(int key) {
        ds->prefill(key, key);
    }

    virtual void do_insert(int key) {
        ds->put(key, key);
    }

    virtual void do_delete(int key) {
        ds->remove(key);
    }

    virtual void do_lookup(int key) {
        ds->lookup(key);
    }

    virtual void do_sync() {
        ds->sync();
    }

    virtual void post_init(int tid) {
        int coreid = (tid % 20) * 2 + (tid / 20);
        pds::EpochSys::init_thread(tid);
        cpu_set_t cpuset;
        CPU_ZERO(&cpuset);
        CPU_SET(coreid, &cpuset);
        pthread_setaffinity_np(pthread_self(), sizeof(cpuset), &cpuset);
    }

protected:
    MontageHashMap *ds;
};


#endif  // PERSISTENT_MONTAGEHASHMAP_HPP_
