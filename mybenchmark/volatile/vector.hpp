#ifndef VOLATILE_VECTOR_HPP_
#define VOLATILE_VECTOR_HPP_

#include <mutex>
#include "vector"
#include "../common/base.hpp"

using namespace std;

class VolatileVectorBenchmark : public Benchmark {
public:
    virtual const char *name() {
        return "VolatileVector";
    }

    virtual void do_prefill(int key) {
        ds.push_back(key);
    }

    virtual void do_insert(int key) {
        ds.push_back(key);
    }

    virtual void do_delete(int key) {
        if (ds.size() <= key) key = ds.size() - 1;
        ds.erase(ds.begin()+key);
    }

    virtual void do_lookup(int key) {
        if (ds.size() <= key) key = ds.size() - 1;
        ds[key];
    }

protected:
    vector<int> ds;
    mutex mtx;
};

#endif  // VOLATILE_VECTOR_HPP_
