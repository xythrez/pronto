#ifndef VOLATILE_HASHMAP_HPP_
#define VOLATILE_HASHMAP_HPP_

#include "../common/base.hpp"

#define NUM_BUCKETS 64

using namespace std;

class VolatileHashMap {
public:
    typedef int T;

    struct BucketNode {
        T key = -1;
        T value = -1;
        BucketNode *next = NULL;
    };

    struct Bucket {
        std::mutex mtx;
        BucketNode head;
    } __attribute__ ((aligned (64)));

    void prefill(T key, T value) {
        int bid = hash(key);
        lock_guard<mutex> lock(buckets[bid].mtx);
        BucketNode *curr = &buckets[bid].head;
        while ((curr = curr->next) != NULL) {
            if (curr->key == key) break;
        }
        if (curr != NULL) {
            curr->next = new BucketNode();
            curr->next->key = key;
            curr->next->value = value;
        }
    }

    void put(T key, T value) {
        int bid = hash(key);
        lock_guard<mutex> lock(buckets[bid].mtx);
        BucketNode *curr = &buckets[bid].head;
        while ((curr = curr->next) != NULL) {
            if (curr->key == key) break;
        }
        if (curr != NULL) {
            curr->next = new BucketNode();
            curr->next->key = key;
            curr->next->value = value;
        }
    }

    void remove(T key) {
        int bid = hash(key);
        lock_guard<mutex> lock(buckets[bid].mtx);
        BucketNode *prev = &buckets[bid].head;
        BucketNode *curr = NULL;
        while (prev->next != NULL) {
            if (prev->next->key == key) {
                curr = prev->next;
                prev->next = curr->next;
                delete curr;
                break;
            }
        }
    }

    void lookup(T key) {
        int bid = hash(key);
        BucketNode *curr = &buckets[bid].head;
        while ((curr = curr->next) != NULL) {
            if (curr->key == key) break;
        }
    }

private:
    Bucket buckets[NUM_BUCKETS];
    int hash(T key) {return key % NUM_BUCKETS;};
};


class VolatileHashMapBenchmark : public Benchmark {
public:
    virtual const char *name() {
        return "VolatileHashMap";
    }

    virtual void init() {
        ds = new VolatileHashMap();
    }

    virtual void do_prefill(int key) {
        ds->prefill(key, key);
    }

    virtual void do_insert(int key) {
        ds->put(key, key);
    }

    virtual void do_delete(int key) {
        ds->remove(key);
    }

    virtual void do_lookup(int key) {
        ds->lookup(key);
    }

protected:
    VolatileHashMap *ds;
};

#endif  // VOLATILE_HASHMAP_HPP_
