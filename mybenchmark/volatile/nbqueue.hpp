#ifndef VOLATILE_NBQUEUE_HPP_
#define VOLATILE_NBQUEUE_HPP_

#include <atomic>
#include "../common/base.hpp"

using namespace std;

class VolatileNBQueue {
public:
    typedef int T;
    struct QueueNode;

    struct QueuePtr {
        QueueNode *ptr;
        unsigned int count;
        bool operator==(const QueuePtr &y){
            return ptr == y.ptr && count == y.count;
        }
    };

    struct QueueNode {
        T value = -1;
        atomic<QueuePtr> next;
    };

    VolatileNBQueue() {
        QueueNode *node = new QueueNode();
        node->next = {NULL, 0};
        head = {node, 0};
        tail = {node, 0};
    }

    void prefill(T key) {
        push(key);
    }

    void push(T value) {
        QueueNode *node = new QueueNode();
        QueuePtr last_tail;
        QueuePtr next;
        node->value = value;
        node->next = {NULL, 0};
        while (1) {
            last_tail = tail;
            next = last_tail.ptr->next;
            if (last_tail == tail) {
                if (next.ptr == NULL) {
                    if (tail.load().ptr->next.compare_exchange_strong(next,
                            {node, next.count + 1})) break;
                } else {
                    tail.compare_exchange_weak(last_tail,
                        {next.ptr, last_tail.count + 1});
                }
            }
        }
        tail.compare_exchange_weak(last_tail, {node, last_tail.count + 1});
    }

    T pop() {
        QueuePtr last_head;
        QueuePtr last_tail;
        QueuePtr next;
        T val;
        while (1) {
            last_head = head;
            last_tail = tail;
            next = last_head.ptr->next;
            if (last_head == head) {
                if (last_head.ptr == last_tail.ptr) {
                    if (next.ptr == NULL) return -1;
                    tail.compare_exchange_weak(last_tail,
                        {next.ptr, last_tail.count + 1});
                } else {
                    val = next.ptr->value;
                    if (head.compare_exchange_strong(last_head,
                        {next.ptr, last_head.count + 1})) break;
                }
            }
        }
        free(last_head.ptr);
        return val;
    }

    T peek() {
        return head.load().ptr->value;
    }

private:
    atomic<QueuePtr> head;
    atomic<QueuePtr> tail;
};


class VolatileNBQueueBenchmark : public Benchmark {
public:
    virtual const char *name() {
        return "VolatileNBQueue";
    }

    virtual void init() {
        ds = new VolatileNBQueue();
    }

    virtual void do_prefill(int key) {
        ds->prefill(key);
    }

    virtual void do_insert(int key) {
        ds->push(key);
    }

    virtual void do_delete(int key) {
        ds->pop();
    }

    virtual void do_lookup(int key) {
        ds->peek();
    }

protected:
    VolatileNBQueue *ds;
};

#endif  // VOLATILE_NBQUEUE_HPP_
