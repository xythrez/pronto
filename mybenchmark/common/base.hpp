#ifndef BASE_CONFIG_HPP_
#define BASE_CONFIG_HPP_

#include <pthread.h>
#include <cstddef>
#include <string>
#include <vector>

using namespace std;

class Configuration {
public:
    bool silent = false;
    int threads = 1;
    int mode = -1;
    int prefill = 0;
    int insert_op = 0;
    int delete_op = 0;
    int lookup_op = 0;
    int sync_interval = -1;
    double duration = 2.0;
    char *dist = NULL;
    void parse_dist();
};

class Benchmark {
public:
    void preinit(Configuration *config);
    void run();
    void print_cfg();
    void print_results();
    static void *worker(void *arg);
    virtual void init() {}
    virtual void post_init(int tid) {}
    virtual void register_factory() {}
    virtual void do_prefill(int arg) = 0;
    virtual void do_insert(int arg) = 0;
    virtual void do_delete(int arg) = 0;
    virtual void do_lookup(int arg) = 0;
    virtual void do_sync() {};
    virtual const char *name() = 0;
    virtual bool use_pronto() {
        return false;
    }

protected:
    unsigned int ops = 0;
    unsigned int syncs = 0;
    long op_latency = 0;
    long sync_latency = 0;
    double actual_duration = 0;
    Configuration *cfg;
    pthread_barrier_t barrier;
};

class ThreadArgs {
public:
    unsigned int id;
    Benchmark *benchmark;
};

#endif  // BASE_CONFIG_HPP_
