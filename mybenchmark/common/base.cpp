#include <chrono>
#include <sstream>
#include <random>
#include "savitar.hpp"
#include <iostream>
#include "base.hpp"

void Configuration::parse_dist() {
    char optype;
    unsigned int count;
    string conf(dist);
    stringstream stris(conf);
    while (!stris.eof()) {
        stris >> optype;
        stris >> count;
        switch (optype) {
            case 'p':
                prefill = count;
                break;
            case 'i':
                insert_op = count;
                break;
            case 'd':
                delete_op = count;
                break;
            case 'l':
                lookup_op = count;
                break;
            case 's':
                sync_interval = count;
                break;
        }
    }
}

void Benchmark::preinit(Configuration *config) {
    cfg = config;
    string line;
}

void Benchmark::print_cfg() {
    if (cfg->silent) return;
    printf("Benchmark: %s\n"
           "Threads: %d threads\n"
           "Duration: %lf sec\n"
           "Distribution: %s\n",
           name(),
           cfg->duration,
           cfg->threads,
           cfg->dist);
}

void Benchmark::print_results() {
    int throughput = 0;
    if (actual_duration != 0.0) throughput = ops/actual_duration;
    if (cfg->silent) {
        printf("%s,%s,%d,%lf,%ld,%ld,%d,%d\n",
               name(),
               cfg->dist,
               cfg->threads,
               actual_duration,
               op_latency,
               sync_latency,
               ops,
               throughput);
    } else {
        printf("Operation Latency: %ld\n"
               "Sync Latency: %ld\n"
               "Ops: %d\n"
               "Actual Duration: %lf sec\n"
               "Throughput: %d per sec\n",
               op_latency,
               sync_latency,
               ops,
               actual_duration,
               throughput);
    }
}

void Benchmark::run() {
    unsigned int i;
    void *ret;
    pthread_t threads[cfg->threads];
    ThreadArgs args[cfg->threads];
    pthread_barrier_init(&barrier, NULL, cfg->threads);
    for (i = 0; i < cfg->threads; i++) {
        args[i].id = i;
        args[i].benchmark = this;
    }
    for (i = 0; i < cfg->prefill; i++)
        do_prefill(i);

    for (i = 1; i < cfg->threads; i++) {
        if (use_pronto())
            Savitar_thread_create(&threads[i], NULL, worker, &args[i]);
        else
            pthread_create(&threads[i], NULL, worker, &args[i]);
    }
    worker(args);
    for (i = 1; i < cfg->threads; i++) {
        pthread_join(threads[i], &ret);
    }
    if (ops > 0)
        op_latency /= ops;
    if (syncs > 0)
        sync_latency /= syncs;
    pthread_barrier_destroy(&barrier);
}

void *Benchmark::worker(void *arg) {
    ThreadArgs *args = (ThreadArgs *) arg;
    unsigned int local_ops = 0;
    unsigned int local_syncs = 0;
    unsigned int next_sync = -1;
    unsigned int i = 0;
    unsigned int delete_threshold = args->benchmark->cfg->insert_op;
    unsigned int lookup_threshold = args->benchmark->cfg->delete_op + delete_threshold;
    unsigned int max_threshold = args->benchmark->cfg->lookup_op + lookup_threshold;
    unsigned int sync_interval = args->benchmark->cfg->sync_interval;;
    unsigned int op_chance;
    unsigned int op_key;
    random_device seed;
    default_random_engine rand(seed());
    uniform_int_distribution<int> op_dist(0, max_threshold);
    uniform_int_distribution<int> key_dist(0, args->benchmark->cfg->prefill * 2);
    sync_interval /= 2;
    binomial_distribution<int> sync_dist(sync_interval, 0.5);

    args->benchmark->post_init(args->id);

    pthread_barrier_wait(&args->benchmark->barrier);
    auto start = chrono::high_resolution_clock::now();
    auto end = start + chrono::duration<double>(args->benchmark->cfg->duration);
    chrono::nanoseconds optime = chrono::duration<long>::zero();
    chrono::nanoseconds synctime = chrono::duration<long>::zero();
    chrono::time_point<chrono::high_resolution_clock, chrono::nanoseconds> ostart;
    if (next_sync >= 0)
        next_sync = sync_interval + sync_dist(rand);

    while (end > chrono::high_resolution_clock::now()) {
        if (local_ops == next_sync) {
            ostart = chrono::high_resolution_clock::now();
            args->benchmark->do_sync();
            synctime += chrono::high_resolution_clock::now() - ostart;
            next_sync += sync_interval + sync_dist(rand);
            local_syncs++;
            continue;
        }
        op_chance = op_dist(rand);
        op_key = key_dist(rand);
        if (op_chance > lookup_threshold) {
            ostart = chrono::high_resolution_clock::now();
            args->benchmark->do_lookup(op_key);
            optime += chrono::high_resolution_clock::now() - ostart;
        } else if (op_chance > delete_threshold) {
            ostart = chrono::high_resolution_clock::now();
            args->benchmark->do_delete(op_key);
            optime += chrono::high_resolution_clock::now() - ostart;
        } else {
            ostart = chrono::high_resolution_clock::now();
            args->benchmark->do_insert(op_key);
            optime += chrono::high_resolution_clock::now() - ostart;
        }
        local_ops++;
    }
    pthread_barrier_wait(&args->benchmark->barrier);
    if (args->id == 0) args->benchmark->do_sync();
    end = chrono::high_resolution_clock::now();
    chrono::duration<double> duration = end - start;
    if (args->id == 0) args->benchmark->actual_duration = duration.count();
    __sync_fetch_and_add(&args->benchmark->ops, local_ops);
    __sync_fetch_and_add(&args->benchmark->syncs, local_syncs);
    __sync_fetch_and_add(&args->benchmark->op_latency, optime.count());
    __sync_fetch_and_add(&args->benchmark->sync_latency, synctime.count());
    return NULL;
}
