#include <unistd.h>
#include <getopt.h>
#include <cstdio>
#include <cstddef>
#include <cstdlib>
#include <string>
#include <iostream>
#include <vector>
#include "common/base.hpp"
#include "volatile/vector.hpp"
#include "volatile/hashmap.hpp"
#include "volatile/nbqueue.hpp"
#include "persistent/hashmap.hpp"
#include "persistent/vector.hpp"
#include "persistent/nbqueue.hpp"
#include "persistent/montagehashmap.hpp"
#include "persistent/montagenbqueue.hpp"

using namespace std;

vector<Benchmark *> benchmarks = {
    new VolatileVectorBenchmark(),
    new VolatileHashMapBenchmark(),
    new VolatileNBQueueBenchmark(),
    new NvVectorBenchmark(),
    new NvHashMapBenchmark(),
    new NvNBQueueBenchmark(),
    new MontageHashMapBenchmark(),
    new MontageNBQueueBenchmark(),
};
Benchmark *bench;

void print_usage(FILE *fd, char *prog) {
    int i;
    fprintf(fd, "usage: %s [-t threads] [-d duration] [-m mode] [-c pXiXdXlX] [-s]"
                "\n"
                "arguments:\n"
                "  -h, --help        show program help\n"
                "  -s, --simple      use simple test output format\n"
                "  -t, --threads     number of worker threads\n"
                "  -d, --duration    minimum test duration in seconds\n"
                "  -m, --mode        data structure to use\n"
                "  -c, --config      the benchmark config (pX; iX; dX; lX; sX)\n"
                "\n"
                "modes:\n", prog);
    for (i = 0; i < benchmarks.size(); i++) {
        fprintf(fd, "  %d - %s\n", i, benchmarks[i]->name());
    }
}

int benchmark_main(int argc, char **argv) {
    bench->print_cfg();
    bench->init();
    bench->run();
    bench->print_results();
    return 0;
}

int main(int argc, char *argv[]) {
    int idx = 0;
    int c;
    Configuration cfg;

    static struct option options[] = {
        {"help",     no_argument,       0, 'h'},
        {"simple",   no_argument,       0, 's'},
        {"threads",  required_argument, 0, 't'},
        {"duration", required_argument, 0, 'd'},
        {"mode",     required_argument, 0, 'm'},
        {"config",   required_argument, 0, 'c'},
    };
    while ((c = getopt_long(argc, argv, "hst:d:m:c:p:", options, &idx)) != -1) {
        switch (c) {
            case 'h':
                print_usage(stdout, argv[0]);
                return 0;
            case 's':
                cfg.silent = true;
                break;
            case 't':
                cfg.threads = atoi(optarg);
                if (cfg.threads < 1) cfg.threads = 1;
                break;
            case 'd':
                cfg.duration = atof(optarg);
                if (cfg.duration < 0.1) cfg.duration = 2.0;
                break;
            case 'm':
                cfg.mode = atoi(optarg);
                if (cfg.mode < 0 || cfg.mode >= benchmarks.size()) cfg.mode = -1;
                break;
            case 'c':
                cfg.dist = optarg;
                cfg.parse_dist();
                break;
            default:
                print_usage(stdout, argv[0]);
                return 1;
        }
    }
    if (cfg.mode == -1 || cfg.dist == NULL) {
        print_usage(stdout, argv[0]);
        return 1;
    }

    bench = benchmarks[cfg.mode];
    bench->register_factory();
    bench->preinit(&cfg);
    if (bench->use_pronto()) {
        Savitar_main(benchmark_main, argc, argv);
    } else {
	    benchmark_main(argc, argv);
    }
}
